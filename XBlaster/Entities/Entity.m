//
//  Entity.m
//  XBlaster
//
//  Created by Brandon Levasseur on 10/27/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "Entity.h"

@implementation Entity

-(instancetype)initWithPosition:(CGPoint)position
{
    if (self = [super init]) {
        self.texture = [[self class] generateTexture];
        self.size = self.texture.size;
        self.position = position;
        _direction = CGPointZero;
    }
    
    return self;
}

-(void)update:(CFTimeInterval)delta
{
    //Overridden by subclasses
}

+(SKTexture *)generateTexture
{
    //Overridden by subclasses
    return nil;
}

@end
