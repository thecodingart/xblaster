//
//  Entity.h
//  XBlaster
//
//  Created by Brandon Levasseur on 10/27/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Entity : SKSpriteNode

@property (assign, nonatomic) CGPoint direction;
@property (assign, nonatomic) float health;
@property (assign, nonatomic) float maxHealth;

+(SKTexture *)generateTexture;
-(instancetype)initWithPosition:(CGPoint)position;
-(void)update:(CFTimeInterval)delta;

@end
